import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.UUID; 


import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;


import org.apache.log4j.PropertyConfigurator;
public class FreesiwtchConnector {
	private static Logger logger = Logger.getLogger(FreesiwtchConnector.class);
	private final String FS_HOST = "FS_HOST";
	private final String FS_PASSWORD = "FS_PASSWORD";
	private final String FS_PORT = "FS_PORT";
	private final  String STATUS_CHECK_SLEEP_TIME="STATUS_CHECK_SLEEP_TIME";
	private final String REGISTER_GATEWAY="REGISTER_GATEWAY";
	private final String CALLING_NUMBER="CALLING_NUMBER";
	private final String  SW_PROJECT_KEY ="SW_PROJECT_KEY";
	private final String  SW_TOKEN ="SW_TOKEN";


        private static final String SMTP_SERVER = "SMTP_SERVER";
        private static final String SMTP_USERNAME = "SMTP_USERNAME";
        private static final String SMTP_PASSWORD = "SMTP_PASSWORD";
        private static final String SMTP_PORT ="SMTP_PORT";
    
   	private static final String EMAIL_FROM = "EMAIL_FROM";
    	private static final String EMAIL_TO = "EMAIL_TO";


	
	public static String fsHost;
	public static int fsPort;
	public static String fsPassword;
	public static String registerGateway;
	public static String callingNumber;
	public static String swProjectKey;
	public static String swToken;
	public static int statusCkeckSleepTime;
	



	
	public static HashMap<String, CallData> callDataMap= new HashMap<String, CallData>();
	
	public static UUID calluuid=null;
	public static boolean isSipRgisterSend =false;
	public static boolean isSipRgisterSuccess =false;
	public static boolean isFreeswichConnected =false;
	
	public static BlockingQueue<ByteBuffer> eventQueue = new LinkedBlockingQueue<>();
	public static BlockingQueue<CallData> callDataQueue = new LinkedBlockingQueue<>();
	public FsClient fsClient = new FsClient();
	

	public static String smtpServer;
	public static String smtpUsername;
	public static String  smtpPassword;
	public static String smtpPort;
	public static String emailTo;
	public static String emailFrom;

	public static void main(String[] args) {
		
		FreesiwtchConnector freesiwtchConnector = new FreesiwtchConnector();
		freesiwtchConnector.init(args);
		freesiwtchConnector.start();
	
	}
	public void init(String[] args) {
		
		try {
			BasicConfigurator.configure();
			Properties config = new Properties();
			config.load(new FileInputStream("../config/config.prop"));
			logger.info("Service Starting...");
			PropertyConfigurator.configure("../config/log4j.prop");
			
			fsHost = config.getProperty(FS_HOST);
			fsPassword = config.getProperty(FS_PASSWORD);
			fsPort = Integer.parseInt(config.getProperty(FS_PORT));
			statusCkeckSleepTime = Integer.parseInt(config.getProperty(STATUS_CHECK_SLEEP_TIME));
			callingNumber = config.getProperty(CALLING_NUMBER);
			swProjectKey=config.getProperty(SW_PROJECT_KEY);
			swToken=config.getProperty(SW_TOKEN);
			registerGateway = config.getProperty(REGISTER_GATEWAY);

			smtpServer =config.getProperty(SMTP_SERVER);
			smtpUsername=config.getProperty(SMTP_USERNAME);
			smtpPassword=config.getProperty(SMTP_PASSWORD);
			smtpPort=config.getProperty(SMTP_PORT);
			emailTo=config.getProperty(EMAIL_TO);
			emailFrom=config.getProperty(EMAIL_FROM);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	class Originator implements Runnable{
		public void run() {
			FreesiwtchConnector.isFreeswichConnected =false;
			boolean sendCallCmmand=false;
			while(true) {
				try {
					Thread.sleep(1000);
						if(FreesiwtchConnector.isFreeswichConnected == true) {
							if(FreesiwtchConnector.isSipRgisterSuccess == true) {

								UUID calluuid = UUID.randomUUID();
								CallData callData = new CallData(calluuid.toString(),false,false,"");
								FreesiwtchConnector.callDataMap.put(calluuid.toString(), callData);
								ByteBuffer sndbyteBuffer = ByteBuffer.allocate(1000);
								// System.out.println("send call command");
								String callCmd="bgapi originate {test_sip_endpoint=true,origination_uuid="+ calluuid+"}sofia/gateway/" + registerGateway + "/" + callingNumber + "  &transfer('status_check XML default') \n\n";
								logger.info("CallUUID: " + calluuid +  " send call command " + callCmd);
								sndbyteBuffer.put(callCmd.getBytes());
								sndbyteBuffer.flip();
								FsClient.socketChannel.write(sndbyteBuffer);

							}
			                 	         // System.out.println("Sending commnad");
                                                        ByteBuffer regByteBuffer = ByteBuffer.allocate(1000);
                                                        String regCmd="api sofia profile external register "+ registerGateway +" \n\n";
                                                        //System.out.println(regCmd);
                                                        regByteBuffer.put(regCmd.getBytes());
                                                        regByteBuffer.flip();
                                                        FsClient.socketChannel.write(regByteBuffer);
                                                        logger.info("sending  register command " + regCmd);

						}

					Thread.sleep(FreesiwtchConnector.statusCkeckSleepTime);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Error sending command to freeswitch",e);
				}
			}
		}
	}
	public void start() {
		
		SendNotification sendNotification = new SendNotification();
		Thread sendNotificationTH = new Thread(sendNotification);
		sendNotificationTH.start();
		
		Originator originator = new Originator();
		Thread originatorTh = new Thread(originator);
		originatorTh.start();
		logger.info("Starting Fs Clinet...");
		FsEventHandler fsEventHandler = new FsEventHandler();
		Thread fsEventHandlerTh = new Thread(fsEventHandler);
		fsEventHandlerTh.start();
		logger.info("Starting Fs Clinet...");
		fsClient.connect(fsHost, fsPort);
	}
}
