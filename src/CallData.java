
public class CallData {
	public String callUUID;
	public boolean callSuccess;
	public boolean digitsSuccess;
	public boolean iscallCompleted;
	public String endReason;
	public CallData(String callUUID, boolean callSuccess,boolean  digitsSuccess, String endReason) {
		this.callUUID = callUUID;
		this.callSuccess = callSuccess;
		this.digitsSuccess = digitsSuccess;
		this.endReason=endReason;
	} 
}
