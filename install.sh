#!/bin/bash
if [ ! -d /usr/local/statusmonitor ]; then
    mkdir -p /usr/local/statusmonitor
fi

javac -cp .:./lib/javax.mail-1.6.2.jar:./lib/activation-1.1.1.jar:./lib/log4j-1.2.17.jar:./lib/javax.mail-api-1.6.2.jar ./src/*.java   ./src/FreesiwtchConnector.java
rm -rf /usr/local/statusmonitor/src
cp -r ./src/ /usr/local/statusmonitor/
cp -r ./lib/ /usr/local/statusmonitor/
#cp -r ./config /usr/local/statusmonitor/
if [ ! -d /usr/local/statusmonitor/config ]; then
    cp -r ./config /usr/local/statusmonitor/
fi

cp ./statusmonitor.service  /etc/systemd/system/statusmonitor.service
systemctl daemon-reload
systemctl enable statusmonitor.service
#systemctl start statusmonitor.service
#systemctl status statusmonitor.service
