import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import org.apache.log4j.Logger;
public class FsClient {
	public static SocketChannel socketChannel = null;
	   public static boolean isFsSocketConnected =false;
	    private static final Logger logger = Logger.getLogger(FsClient.class);
	    public void connect(String fsHost,int fsPort) {
	    	try{
	            logger.info("At connectToSocketChannel - host :" + fsHost +", port :"+ fsPort);
	            socketChannel = SocketChannel.open();
	            socketChannel.connect(new InetSocketAddress(fsHost, fsPort));
	            isFsSocketConnected=true;
	            while(true){
	                ByteBuffer byteBuffer = ByteBuffer.allocate(15000);
	                this.read(byteBuffer);
	                byteBuffer = null;
	            }
	        }catch (IOException e){
	        	isFsSocketConnected=false;
	        	logger.info("Error creating freeswtch connection...",e);
	        }
	    }
	    private void read(ByteBuffer byteBuffer) {
	    	 int totalBytesRead = 0;
	         try{
	             int bytesRead = socketChannel.read(byteBuffer);
	             while(bytesRead <= 0){
	                 byteBuffer.clear();
	             }
	            totalBytesRead += bytesRead;
	         }catch (IOException e){
	        	 logger.info("Error reading socket data.. ",e);
	         }
	         if(totalBytesRead > 0){
	        	 this.addToQueue(byteBuffer);
	         }
			
		}
	    private void addToQueue(ByteBuffer receivedBytes){
	        try{
	        	FreesiwtchConnector.eventQueue.add(receivedBytes);
	        }catch (Exception e){
	        	logger.error("Error Adding even data in queue...  ",e);
	        }
	    }
}
