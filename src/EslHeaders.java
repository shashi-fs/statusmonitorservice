
public class EslHeaders {
	 
            public static final String OK_ACCEPTED = "+OK accepted";
            public static final String OK_EVENTS_ENABLED = "+OK event listener enabled plain";
            /**
             * {@code "auth/request"}
             */
            public static final String AUTH_REQUEST = "auth/request";
            /**
             * {@code "api/response"}
             */
            public static final String API_RESPONSE = "api/response";
            /**
             * {@code "command/reply"}
             */
            public static final String COMMAND_REPLY = "command/reply";
            /**
             * {@code "text/event-plain"}
             */
            public static final String TEXT_EVENT_PLAIN = "text/event-plain";
            /**
             * {@code "text/event-xml"}
             */
            public static final String TEXT_EVENT_XML = "text/event-xml";
            /**
             * {@code "text/disconnect-notice"}
             */
            public static final String TEXT_DISCONNECT_NOTICE = "text/disconnect-notice";
            /**
             * {@code "-ERR invalid"}
             */
            public static final String ERR_INVALID = "-ERR invalid";
            public static final String REPLY_TEXT = "Reply-Text";
            public static final String CONTENT_TYPE = "Content-Type";
            public static final String OK_EVENT_LISTENER_ENABLED_PLAIN = "+OK event listener enabled plain";
            
}
