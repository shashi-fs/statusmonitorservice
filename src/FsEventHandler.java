import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.log4j.Logger;



public class FsEventHandler implements Runnable {
	 private Logger logger = Logger.getLogger(FsEventHandler.class);
	 private final String CONTENT_LENGTH = "Content-Length";
	 private final String DBLLINE_TERMINATOR = "\n\n";
	 private final String TEXT_EVENT_PLAIN = "text/event-plain";
	 private static final String CONTENT_TYPE = "Content-Type";
	 private final String EVENT_NAME = "Event-Name";
	 private final String BACKGROUND_JOB = "BACKGROUND_JOB";
	 private final String CHANNEL_HANGUP_COMPLETE = "CHANNEL_HANGUP_COMPLETE";
	 private final String CHANNEL_EXECUTE_COMPLETE = "CHANNEL_EXECUTE_COMPLETE";
	 private final String CUSTOM = "CUSTOM";
	 private final String REGED ="REGED";  
	 private final String FAILED = "FAILED";
	 private  final String STATE="State";
	 private final String GATEWAY = "Gateway";
	 private final String CALL_UUID = "Unique-ID";
	 private final String DIGITS = "variable_pagd_digits";
	 private final String END_REASON = "Hangup-Cause";
	 private final String APPLICATION = "Application";
	 private final String PLAY_AND_GET_DIGITS="play_and_get_digits";
	 private final String TEST_SIP_ENDPONT="variable_test_sip_endpoint";
	 private final String PING_STATUS="Ping-Status";
	 private final String DOWN="DOWN";
	 private final String TRUE="true";

	  private final String START_TIME = "variable_answer_epoch";
	  private final String END_TIME = "variable_end_epoch";
	  private void sendMessage(String message) {
		   SendNotification sn =new SendNotification();
		   sn.sendMessage(message, null);
	   }
	   private char[] getProperChunk(char[] ch) {
	    	 StringBuilder prpData = new StringBuilder();
	    	for(int i=0;i< ch.length;i++) {
	    		if((int)ch[i]!=0 ) {
	    			prpData.append(ch[i]);
	    		}
	    	}
	    	return prpData.toString().toCharArray();
	    }
	    private void sendEmail(String message, String subject) {
		   SendNotification sn =new SendNotification();
		   sn.sendEmail(message, subject);
	   }
	   private void doAuthentication() {
	        ByteBuffer sndbyteBuffer = ByteBuffer.allocate(100);
	        StringBuilder cmd = new StringBuilder();

	        try {
	            cmd.append("auth ");
	            cmd.append(FreesiwtchConnector.fsPassword);
	            cmd.append(DBLLINE_TERMINATOR);
	            sndbyteBuffer.put(cmd.toString().getBytes());
	            sndbyteBuffer.flip();
	            FsClient.socketChannel.write(sndbyteBuffer);
	            //System.out.println("Seding authentication to freeswotch...");
	            logger.info("Seding authentication to freeswotch...  " + cmd.toString());
	        } catch (Exception e) {
	            logger.error("Error in authentication -->", e);
	        }
	    }
	   private void AddUpDateDbQueue(EslMessage eslMsg) {
		   java.util.Map<String, String> eventData = eslMsg.getHeaders();
//		   for (Entry<String, String> entry : eventData.entrySet())  
//	        	System.out.println("Key = " + entry.getKey() + 
//	                             ", Value = " + entry.getValue());
		   
		   String CallUUID;
		   String digits;
		   String endReason;
		   long startTime;
	       long endTime;
		   if (eventData.get(EVENT_NAME).equals(CUSTOM)){
			   if(eventData.get(GATEWAY).equals(FreesiwtchConnector.registerGateway)){
				   if(eventData.get(STATE).equals(REGED)) {
					   FreesiwtchConnector.isSipRgisterSuccess=true;
					logger.info(FreesiwtchConnector.registerGateway + " Registration Success");
				   }else if(eventData.get(STATE).equals(FAILED)) {
					FreesiwtchConnector.isSipRgisterSuccess=false;
					  sendEmail(FreesiwtchConnector.registerGateway +" Gateway down ","EndPoint registration failed");
				   }else if(eventData.get(PING_STATUS).equals(DOWN)) {
					  FreesiwtchConnector.isSipRgisterSuccess=false;
					   sendEmail(FreesiwtchConnector.registerGateway +" Gateway ping status Failed ","EndPoint registration failed");
				   }
			   }
		   }else if(eventData.get(EVENT_NAME).equals(CHANNEL_EXECUTE_COMPLETE) && eventData.get(APPLICATION).equals(PLAY_AND_GET_DIGITS)) {
			   if(eventData.get(TEST_SIP_ENDPONT).equals(TRUE)){
			         CallUUID =eventData.get(CALL_UUID);
			         digits = (eventData.get(DIGITS) == null ? "" :  eventData.get(DIGITS));
				     //System.out.println("Got  CHANNEL_EXECUTE_COMPLETE digits   " + eventData.get(DIGITS) +"  var "  + digits);
				     synchronized(FreesiwtchConnector.callDataMap) {
				    	 CallData  callData=  FreesiwtchConnector.callDataMap.get(CallUUID);
				    	 if(digits.equals(digits)) {
				    		 //System.out.println("Digits success");
				    		 callData.digitsSuccess=true;
				    	 }
				    	 FreesiwtchConnector.callDataMap.put(CallUUID, callData);
				    	 //System.out.println("********* " + callData.digitsSuccess);
				     }
			}
		   }else if(eventData.get(EVENT_NAME).equals(CHANNEL_HANGUP_COMPLETE)) {
			if(eventData.get(TEST_SIP_ENDPONT).equals(TRUE)){
			   //System.out.println("*****************************************************************");
			   CallUUID =eventData.get(CALL_UUID);
			   endReason =eventData.get(END_REASON);
		    	startTime = eventData.get(START_TIME) == null ? 0 :  Long.parseLong(eventData.get(START_TIME));
				endTime =eventData.get(END_TIME) == null ? 0 :  Long.parseLong(eventData.get(END_TIME));
			   synchronized(FreesiwtchConnector.callDataMap) {
			    	 CallData  callData=  FreesiwtchConnector.callDataMap.get(CallUUID);
			    	 CallData hangupCallData = new CallData("",false,false,"");
			    	 if(startTime>0) {
			    		 hangupCallData = new CallData(CallUUID,true,callData.digitsSuccess,endReason);
			    	 }else {
			    		 hangupCallData = new CallData(CallUUID,false,callData.digitsSuccess,endReason);
			    	 }
			    	 //System.out.println("Adding Data to callData Queue");
			    	 FreesiwtchConnector.callDataQueue.add(hangupCallData);
			     }
			 }
		   }
	   }
	   private void EnableReqiredEvents() {
	        ByteBuffer sndbyteBuffer = ByteBuffer.allocate(1000);
	        StringBuilder evCmd = new StringBuilder();
	        try {
	            evCmd.append("event plain CHANNEL_HANGUP_COMPLETE CHANNEL_EXECUTE_COMPLETE CUSTOM sofia::gateway_state");
	            evCmd.append(DBLLINE_TERMINATOR);

	            sndbyteBuffer.put(evCmd.toString().getBytes());
	            sndbyteBuffer.flip();
	            logger.info("Seding EnableReqiredEvents  to freeswotch...  " + evCmd.toString());
	            FsClient.socketChannel.write(sndbyteBuffer);
	        } catch (Exception e) {
	            logger.error("Error in enabling events -->", e);
	        }
	    }
	   private void doEslContentType(EslMessage eslMsg) {
	        //System.out.println("COntent Type " +eslMsg.getContentType());
	        try {
	            switch (eslMsg.getContentType()) {
	            case EslHeaders.AUTH_REQUEST:
	            	  System.out.println("COntent Type AUTH_REQUEST -- " +eslMsg.getContentType());
	                doAuthentication();
	                break;
	            case EslHeaders.COMMAND_REPLY:
	            	 //System.out.println("COntent Type COMMAND_REPLY -- " +eslMsg.getContentType());
	                if (eslMsg.getReplyIsOK()) {
	                	System.out.println("Freeswitch ESL Connected suffefully");
	                	FreesiwtchConnector.isFreeswichConnected=true;
	                	EnableReqiredEvents();	
	                }
	                if(eslMsg.getEventsEnabledOK()) {
				logger.info("Events Enabled Successfully...");
			}
	                break;
	            case EslHeaders.TEXT_EVENT_PLAIN:
	            	//System.out.println("COntent Type TEXT_EVENT_PLAIN -- " +eslMsg.getContentType());
	                AddUpDateDbQueue(eslMsg);
	                break;
	            }
	        } catch (Exception e) {
	            logger.error("**********Error In doEslContentType Please check the Sorce code", e);
	            //System.out.println(eslMsg.toString() + e.toString());
	        }
	    }
	 public void run() {
	        ByteBuffer byteBuffer = null;
	        int colonIndex = 0;
	        HashMap<String, String> headers = new HashMap<String, String>();
	        String raw_body = null;
	        StringBuilder line = new StringBuilder();
	        char[] ch = null;
	        while (true) {
	            try {
	                byteBuffer = FreesiwtchConnector.eventQueue.take();
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	            // ***********************Parse ESL Stream***********************************
	            String byteData = new String(byteBuffer.array());
	            ch = getProperChunk(byteData.toCharArray());
	            //System.out.println(byteData);
	            for (int i = 0; i < ch.length; i++){
	                if(ch[i] !=0) {
	                    if (ch[i] != 10) {
	                        if (colonIndex == 0 && ch[i] == 58) {
	                              colonIndex = line.length();
	                            }
	                          line.append((char) ch[i]);
	                        } else if (line.length() == 0) {
	                            //********************************When Content Length*******************************
	                            if (headers.containsKey(CONTENT_LENGTH)) {
	                                int cLength = Integer.parseInt(headers.get(CONTENT_LENGTH));
	                                int cIndex = 0;
	                                i++;
	                                while (true) {
	                                  if (headers.get(CONTENT_TYPE).equals(TEXT_EVENT_PLAIN)) {
	                                    for (; i < ch.length && cIndex < cLength; cIndex++, i++) {
	                                      if (ch[i] != 10) {
	                                        if (colonIndex == 0 && ch[i] == 58)
	                                          colonIndex = line.length();
	                                        line.append((char) ch[i]);
	                                      } else if (line.length() != 0) {
	                                        headers.put(line.substring(0, colonIndex), line.substring(colonIndex + 1).trim());
	                                        colonIndex = 0;
	                                        line.delete(0, line.length());
	                                      } else if (headers.get(EVENT_NAME).equals(BACKGROUND_JOB)) {
	                                        i++;
	                                        cIndex++;
	                                        while (true) {
	                                          for (; i < ch.length && cIndex < cLength; cIndex++, i++) {
	                                            if (ch[i] != 10)
	                                              line.append((char) ch[i]);
	                                          }
	                                          if (cIndex < cLength) {
	                                            try {
	                                                byteBuffer=FreesiwtchConnector.eventQueue.take();
	                                                  String byteData1 = new String(byteBuffer.array());
	                                                     ch = getProperChunk(byteData1.toCharArray());
	                                            } catch (InterruptedException e) {
	                                              break;
	                                            }
	                                            i = 0;
	                                          } else {
	                                            i--;
	                                            cIndex--;
	                                            break;
	                                          }
	                                        }
	                                        raw_body = line.toString();
	                                      }
	                                    }
	                                    if (cIndex < cLength) {
	                                      try {
	                                          byteBuffer=FreesiwtchConnector.eventQueue.take();
	                                          String byteData2 = new String(byteBuffer.array());
	                                             ch = getProperChunk(byteData2.toCharArray());
	                                      } catch (InterruptedException e) {
	                                        break;
	                                      }
	                                      i = 0;
	                                    } else {
	                                      i--;
	                                      cIndex--;
	                                      break;
	                                    }
	                                  } else {
	                                    for (; i < ch.length && cIndex < cLength; cIndex++, i++) {
	                                      line.append((char) ch[i]);
	                                    }
	                                    if (cIndex < cLength) {
	                                      try {
	                                          byteBuffer=FreesiwtchConnector.eventQueue.take();
	                                          String byteData3 = new String(byteBuffer.array());
	                                             ch = getProperChunk(byteData3.toCharArray());
	                                      } catch (InterruptedException e) {
	                                        break;
	                                      }
	                                      i = 0;
	                                    } else {
	                                      i--;
	                                      cIndex--;
	                                      raw_body = line.toString();
	                                      break;
	                                    }
	                                    // raw_body = line.toString();
	                                  }
	                                }
	                            }
	                            //***********************************************************************************
	                          doEslContentType(new EslMessage(headers, raw_body));
	                          headers = new HashMap<String, String>();
	                          raw_body = null;
	                          colonIndex = 0;
	                          line.delete(0, line.length());
	                        } else {
	                            //System.out.println(line.toString().trim());
	                        //    System.out.println(colonIndex);
	                          headers.put(line.substring(0, colonIndex), line.substring(colonIndex + 1).trim());
	                          colonIndex = 0;
	                          line.delete(0, line.length());
	                        }
	                }
	              }
	            // **********************************************************

	        }
	    }
}
