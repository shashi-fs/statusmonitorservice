import java.util.HashMap;
import java.util.Map;

public class EslMessage {
	private HashMap<String, String> headers = new HashMap<String, String>();
	private String body;
	EslMessage(HashMap<String, String> _headers, String _body) {
		headers = _headers;
		body = _body;
	  }
	public Map<String, String> getHeaders() {
		return headers;
	  }
	public boolean hasHeader(String headerName) {
		return headers.containsKey(headerName);
	  }
	public String getHeaderValue(String headerName) {
		return headers.get(headerName);
	  }
	public String getReplyText() {
		return headers.get(EslHeaders.REPLY_TEXT);
	  }
	public boolean getReplyIsOK() {
		return headers.get(EslHeaders.REPLY_TEXT).startsWith(EslHeaders.OK_ACCEPTED);
	  }
	public boolean getEventsEnabledOK() {
		return headers.get(EslHeaders.REPLY_TEXT).startsWith(EslHeaders.OK_EVENTS_ENABLED);
	  }
	public String getContentType() {
		return headers.get(EslHeaders.CONTENT_TYPE);
	  }
	public String getBody() {
		return body;
	  }
	@Override
	  public String toString() {
		StringBuilder sb = new StringBuilder("EslMessage: contentType=[");
		sb.append(headers.get("Content-Type"));
		sb.append("] headers=");
		sb.append(headers.size());
		sb.append(", body=");
		sb.append(body);
		return sb.toString();
	  }
}
