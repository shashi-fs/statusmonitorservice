import org.apache.log4j.Logger;

import com.sun.mail.smtp.SMTPTransport;

import javax.mail.Message;
//import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;


public class SendNotification implements Runnable{
	private static Logger logger = Logger.getLogger(SendNotification.class);
	
	public void run() {
		logger.info("Starting Send Notification Thread");
		while(true) {
			try {
				Thread.sleep(10);
				CallData callData = FreesiwtchConnector.callDataQueue.take();
				if(callData != null) {
					System.out.println("Got Call Data....");
					logger.info(callData.callUUID + "  EndReason : " + callData.endReason + "  CallSucces : " + callData.callSuccess + "  Digits Success : " + callData.digitsSuccess);
					if(!callData.callSuccess) {
						sendEmail("Call Failed wirh Reason "  + callData.endReason,"Call Failed notifigation");
					}
					if(!callData.digitsSuccess) {
						sendEmail("Call Failed wirh Reason "  + callData.endReason + " and No Digits received ","Digits failed notification");
					}
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
		}
	public void sendMessage(String Message,CallData callData ){
		String alertMessage;
		if(callData == null) {
			alertMessage=Message;
			logger.info(Message);
		}else {
			alertMessage=callData.callUUID + "  " + Message;
			logger.info(alertMessage);	
		}
	}
	public void sendEmail(String message,String subject){

		Properties prop = System.getProperties();
	        prop.put("mail.smtp.host", FreesiwtchConnector.smtpServer); 
        	prop.put("mail.smtp.auth", "true");
	        prop.put("mail.smtp.port", FreesiwtchConnector.smtpPort); // default port 25

        	Session session = Session.getInstance(prop, null);
	        Message msg = new MimeMessage(session);

        	try {	
	            msg.setFrom(new InternetAddress(FreesiwtchConnector.emailFrom));
        	    msg.setRecipients(Message.RecipientType.TO,
	            InternetAddress.parse(FreesiwtchConnector.emailTo, false));
        	    //msg.setRecipients(Message.RecipientType.CC,
	            //InternetAddress.parse(EMAIL_TO_CC, false));
        	    msg.setSubject(subject);
	            msg.setText(message);
        	    msg.setSentDate(new Date());
	            SMTPTransport t = (SMTPTransport) session.getTransport("smtp");
        	    t.connect(FreesiwtchConnector.smtpServer, FreesiwtchConnector.smtpUsername, FreesiwtchConnector.smtpPassword);
	            t.sendMessage(msg, msg.getAllRecipients());

        	    logger.info("Response: " + t.getLastServerResponse());

            t.close();

        } catch (Exception e) {
		logger.info("Error while sending email .. ",e);
        }
		
	}
}


